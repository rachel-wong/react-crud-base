// import { useState } from 'react'

// const dummytasks = [
//   {
//     id: 1,
//     text: 'Appt 1',
//     day: 'Feb 5th at 2:30pm',
//     reminder: true
//   },
//   {
//     id: 2,
//     text: 'Appt 3',
//     day: 'Feb 10th at 16:30pm',
//     reminder: true
//   },
//   {
//     id: 3,
//     text: 'Appt 3',
//     day: 'Mar 5th at 12:30pm',
//     reminder: false
//   }
// ]

import Task from './Task'

const Tasks = ({ tasks, onDelete, onToggle }) => {

  // const [tasks, setTasks] = useState(dummytasks)

  return (
    // setTasks({..tasks}, {newTaskObject}) // adding a new task. not tasks.push(newTaskObject)
    <>
      {/* {tasks.map((task) => (
        <h3 key={task.id}>{ task.text }</h3>
      ))} */}

      {tasks.map((task) => (
        <Task key={task.id} task={task} onDelete={onDelete} onToggle={ onToggle }/>
      ))}
    </>
  )
}

export default Tasks
