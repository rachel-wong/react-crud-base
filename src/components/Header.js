import Button from './Button'
import { useLocation } from 'react-router-dom'

const Header = ({ onAdd, showAddTask }) => {
  // const onClick = () => {
  //     console.log("Clicked outside")
  //   }

  const location = useLocation() // checks which route the user has navigated to

  return (
    <header className="header">
      <h1>Task Runner</h1>
      {location.pathname === '/' && <Button color={showAddTask ? `cornflowerblue` : `peach`} text={showAddTask ? `Close` : `Add task`} onClick={onAdd} />}
    </header>
  )
}

export default Header