import { useState } from 'react'
// each input will have its own state here at component-level

const AddTask = ({ onAdd }) => {
  const [text, setText] = useState('')
  const [day, setDay] = useState('')
  const [reminder, setReminder] = useState(false)

  const onSubmit = (e) => {
    e.preventDefault();

    if (!text) {
      alert('Please add a task')
      return;
    }

    // passes an object that has text day and reminder to onAdd
    onAdd({ text, day, reminder })

    // clear form
    setText('')
    setDay('')
    setReminder(false)
  }
  return (
    <form className="add-form" onSubmit={ onSubmit }>
      <div className="form-control">
        <label>Task</label>
        {/* onChange here passes the event value in order to get what's actually inside the input box */}
        <input type="text" placeholder="Add task name" value={text} onChange={ (e) => setText(e.target.value)}/>
      </div>
      <div className="form-control">
        <label>Day and time</label>
        <input type="text" placeholder="Add day and time" value={day} onChange={(e) => setDay(e.target.value)}/>
      </div>
      <div className="form-control form-control-check">
        <label>Reminder</label>

        {/* Not sure what checked={ reminder } does here from an adding task perspective */}
        <input type="checkbox" checked={ reminder } value={reminder} onChange={ (e) => setReminder(e.currentTarget.checked)}/>
      </div>
      <input className="btn btn-block" style={{backgroundColor: 'cornflowerblue'}} type="submit" value="Save Task" />
    </form>
  )
}

export default AddTask
