import PropTypes from 'prop-types'

const Button = ({ color, text, onClick }) => {

  // can declare functions here to use within this component
  // const onClickInside = () => {
  //   console.log("Clicked inside")
  // }

  return (
    <button onClick={ onClick } className="btn" style={{ backgroundColor: color }}>{ text }</button>
  )
}

Button.propTypes = {
  text: PropTypes.string,
  color: PropTypes.string,
  onClick: PropTypes.func
}

Button.defaultProps = {
  color: 'steelblue',
  text: 'Label',
}

export default Button