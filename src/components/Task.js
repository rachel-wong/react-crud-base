import { FaTimes } from 'react-icons/fa'

const Task = ({ task, onDelete, onToggle }) => {
  return (
    <div className={`task ${task.reminder ? 'reminder' : ''}`} onDoubleClick={() => onToggle(task.id)}>
      <h3>{task.text}
        {/* onDelete: instead of just calling the onDelete function, call a functio to call onDelete to pass up the id back to App.js level */}
        <FaTimes style={{ color: 'red', cursor: 'pointer' }} onClick={() => onDelete(task.id)}  /></h3>
      <p>{ task.day }</p>
      <p className="reminder">{ task.reminder }</p>
    </div>
  )
}

export default Task
