import Header from './components/Header'
import Tasks from './components/Tasks'
import AddTask from './components/AddTask'
import Footer from './components/Footer'
import { useEffect, useState } from 'react'
import About from './components/About'
import { BrowserRouter, Route} from 'react-router-dom'
// const dummytasks = [
//   {
//     id: 1,
//     text: 'Appt 1',
//     day: 'Feb 5th at 2:30pm',
//     reminder: true
//   },
//   {
//     id: 2,
//     text: 'Appt 2',
//     day: 'Feb 10th at 16:30pm',
//     reminder: true
//   },
//   {
//     id: 3,
//     text: 'Appt 3',
//     day: 'Mar 5th at 12:30pm',
//     reminder: false
//   }
// ]

function App() {

  const [showAddTask, setShowAddTask] = useState(false)

  const [tasks, setTasks] = useState([])


  useEffect(() => {
    // const fetchTasks = async () => {
    //   const response = await fetch(`http://localhost:5000/tasks`)
    //   const data = await response.json();
    //   console.log("useEffect", data)

    //   setTasks(data)
    // }

    const getTasks = async () => {
      const tasksFromServer = await fetchTasks()
      setTasks(tasksFromServer)
    }
    getTasks()
    // fetchTasks() // remember to call the method
  }, [])

  // Fetch tasks from api endpoint
  // Benefit of this is decouple it from useEffect
  const fetchTasks = async () => {
    const response = await fetch(`http://localhost:5000/tasks`)
    const data = await response.json();
    console.log("useEffect", data)
    return data
  }

  // Get ONE tasks from api endpoint
  const fetchOneTask = async (id) => {

    const response = await fetch(`http://localhost:5000/tasks/${id}`)
    const data = await response.json()
    return data
  }

  // Any state changes need to happen in here at parent level

  // Add Task
  const addTask = async (task) => {

    const response = await fetch(`http://localhost:5000/tasks`, {
      method: "POST",
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(task)
    })

    console.log('response', response)

    // this needs to be await also because addTask itself is async
    const data = await response.json()
    setTasks([...tasks, data])

    // const id = Math.floor(Math.random() * 10000) + 1
    // // console.log(task, id)
    // const newTask = {id, ...task}
    // // setTasks(tasks.push(newTask)) // cannot push new object into the tasks array, must spread existing state and reset the tasks state with a new array of objects
    // setTasks([...tasks, newTask])
    // console.log("final tasks", tasks)
  }

  // Delete Task
  // this function is passed down to Task component
  // async when making a server call
  const deleteTask = async (id) => {
    await fetch(`http://localhost:5000/tasks/${id}`, {
      method: 'DELETE'
    })

    setTasks(tasks.filter(x => x.id !== id))
  }

  // Toggle reminder
  const toggleReminder =  async (id) => {

    // changing the reminder value on server
    const taskToToggle = await fetchOneTask(id) // getting it from server
    const updatedTask = { ...taskToToggle, reminder: !taskToToggle.reminder } // updating the value

    const response = await fetch(`http://localhost:5000/tasks/${id}`, {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(updatedTask)
    }) // updating the matching entry on tasksFromServer

    const data = await response.json()

    // console.log('remind', id)
    // if matched, spread the task and flip the reminder attribute to its opposite
    setTasks(tasks.map((task) => task.id === id ? { ...task, reminder: !data.reminder } : task))
  }

  return (
    <BrowserRouter>
      <div className="container">
        <Header onAdd={() => { setShowAddTask(!showAddTask) }} showAddTask={showAddTask} />

        <Route path='/' render={() => (
          <>
            {/* moving the form inside this route restricts it only to the home/root page. otherwise organise a separate route */}
            {showAddTask && (<AddTask onAdd={addTask} />)}
            {tasks.length > 0 ? (<Tasks tasks={tasks} onDelete={deleteTask} onToggle={toggleReminder} />) : 'No tasks to show'}
          </>
        )} exact />

        <Route path='/about' component={ About } exact />
        <Footer />
        </div>
    </BrowserRouter>
  );
}

export default App;